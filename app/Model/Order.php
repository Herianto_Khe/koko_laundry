<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $table = 'orders';

	protected $fillable = ['name'];

	public function prints(){
		return $this->hasMany('App\Model\Print_data','order_id');
	}

	public function users(){
		return $this->belongsTo('App\Model\User','user_id');
	}
}
