<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Print_data extends Model
{
    protected $table = 'prints';

    protected $fillable = ['order_id','category_name', 'product_name', 'price','quantity','total'];

    public function orders(){
    	return $this->belongsTo('App\Model\Order','order_id');
    }

    public function categories(){
    	return $this->belongsTo('App\Model\Category','category_name');
    }

    public function products(){
    	return $this->belongsTo('App\Model\Product','product_name');
    }
}
