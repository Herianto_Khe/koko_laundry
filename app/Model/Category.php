<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'categories';

	protected $fillable = ['name','id'];

	public function products(){
		return $this->hasmany('App\Model\Product','id_category');
	}
}
