<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Category;

class CategoryController extends Controller
{
    public function home(){ 
        $datas = Category::select('id','name')->get();
        return view('categories.index',['datas'=>$datas]);
    }

    public function store(Request $request){
    	$this->validate($request,
            ['name'	            => 'required',],
            ['name.required'	=> ' Field cannot be empty.',]);
    	Category::create($request->all());
        return redirect()->route('category.index');
    }

    public function edit($id){
        $category = Category::findOrFail($id);
        return view('categories.edit', compact('category'));
    }

    public function update($id, Request $request){
        $this->validate($request,
            ['name'             => 'required',],
            ['name.required'    => ' Field cannot be empty.',]);
        $category = Category::findOrFail($id);
        $category->update($request->all());
        return redirect()->route('category.index');
    }

    public function delete($id){
        $category = Category::findOrFail($id);
        return view('categories.delete', compact('category'));
    }

    public function destroy($id){
        try{
            $category = Category::findOrFail($id);
            $category->delete();
            return redirect()->route('category.index');
            
        }
        catch(\Exception $e){
            $category = Category::findOrFail($id);
            
        } 
        return redirect()->route('category.index');
    }
}
