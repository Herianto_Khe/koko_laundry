<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Category;
use App\Model\Product;
use App\Model\Print_data;
use App\Model\Order;

class OrderController extends Controller
{
    public function login(){
        return view('auth.login');
    }

    public function home(Request $request){
        $datac = Category::all();
        return view('orders.index', compact('datac'));
    }

    public function historyData(Request $request){
        $orders = Order::all(); 
        return view('orders.history',compact('orders'));
    }

    public function findProduct(Request $request){
        $data = Product::where('id_category',$request->id)->get();
        return response()->json($data);
    }

    public function findPrice(Request $request){
        $data = Product::where('id',$request->id)->first();
        return response()->json($data);
    }

    public function printData(Request $request)
    {
        $Order_id = new Order;
        $Order_id->name = $request->orderid;
        $datas= new Print_data;
        if($Order_id->save()){
            foreach ($request->category as $data => $value) 
            {
                $id = $Order_id->id;
                $data = array(
                    'order_id'=>$id,
                    'category_name' => $request->category[$data],
                    'product_name'  => $request->product[$data],
                    'price'         => $request->price[$data],
                    'quantity'      => $request->quantity[$data],
                    'total'         => $request->total[$data],
                    );
                Print_data::insert($data);
            }
        }
        $Order_id->save();
        $orderlist =Print_data::where('order_id',$Order_id->id)->get();
        $total = Print_data::select('total')->where('order_id',$Order_id->id)->get()->sum('total');
        return view('orders.printlist',['datas'=>$orderlist],compact('total'));

    }

    

}

