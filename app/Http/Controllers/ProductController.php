<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\Category;


class ProductController extends Controller
{
    public function home(){
        $datas = Product::select(
            'id',
            'name',
            'price',
            'id_category'
            )->get();
        $prod = Category::pluck('name','id');
        return view('products.index',['datas'=>$datas],compact('prod'));
    }

    public function store(Request $request){
    	
    	Product::create($request->all());
        return redirect()->route('product.index');
    }

    public function edit($id){
        $product = Product::findOrFail($id);
        return view('products.edit', compact('product'));
    } 

    public function update($id, Request $request){
        $product = Product::findOrFail($id);
        $product->update($request->all());
        return redirect()->route('product.index');
    }

    public function delete($id){
        $product = Product::findOrFail($id);
        return view('products.delete', compact('product'));
    }  

    public function destroy($id){
        try{
            $product = Product::findOrFail($id);
            $product->delete();
            return redirect()->route('product.index');
            
        }
        catch(\Exception $e){
            return redirect()->route('product.index');
        } 
    }
}
