<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
	return view('home');
});
Auth::routes();
Route::get('home', 'Controller@welcome')->name('guest.home');
Route::group(['middleware' => 'auth'], function () {
	Route::group(['namespace' => 'Auth'], function () {
		Route::get('/logout', 'LoginController@logout')->name('admin.logout');
	});

	Route::group(['prefix' => 'admins'], function () {
		Route::group(['prefix' => 'categories'], function(){
			Route::get('/', 'CategoryController@home')->name('category.index');
			Route::get('/data', 'CategoryController@data')-> name('category.data');
			Route::post('/store', 'CategoryController@store')->name('category.store');
			Route::get('/{categories}/edit', 'CategoryController@edit')-> name('category.edit');
			Route::patch('/{categories}', 'CategoryController@update');
			Route::get('/{categories}', 'CategoryController@delete')->name('category.delete');
			Route::delete('/{categories}', 'CategoryController@destroy')->name('category.destroy');
		});

		Route::group(['prefix' => 'products'], function(){
			Route::get('/','ProductController@home')->name('product.index');
			Route::get('/data', 'ProductController@data')-> name('product.data');
			Route::post('/product/store', 'ProductController@store')->name('product.store');
			Route::get('/{products}/edit', 'ProductController@edit')-> name('product.edit');
			Route::patch('/{products}', 'ProductController@update');
			Route::get('/{products}', 'ProductController@delete')->name('product.delete');
			Route::delete('/{products}', 'ProductController@destroy')->name('product.destroy');
		});

		Route::group(['prefix' => 'orders'], function(){
			Route::get('/new', 'OrderController@home')->name('order.index');
			Route::post('/print', 'OrderController@printdata')->name('order.print');
			Route::get('/findProduct',array('as'=>'findProduct','uses'=>'OrderController@findProduct'));
			Route::get('/findPrice',array('as'=>'findPrice','uses'=>'OrderController@findPrice'));
			Route::get('/history','OrderController@historydata')-> name('order.history');
		});
	});	
});







