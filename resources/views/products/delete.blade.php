<div class="modal-header">
    <button 
    type="button" 
    class="close" 
    data-dismiss="modal" 
    aria-hidden="true">
    ×
</button>
<h4 class="modal-title">Delete Confirmation</h4>
</div>

<div class="modal-body">
    <p>Apakah anda yakin ingin menghapus?</p>
    <div class="modal-footer" style="height: 40px;">
        {!! Form::open(['method' => 'DELETE', 'route' => ['product.destroy', $product->id]]) !!}
        {!! Form::submit('Ya', ['class' => 'btn btn-danger btn-sm']) !!}
        <button 
        type="button" 
        class="btn btn-sm btn-default" 
        data-dismiss="modal">
        Tidak
    </button>
    {!! Form::close() !!}
</div>
</div>