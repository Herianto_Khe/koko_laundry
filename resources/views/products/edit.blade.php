<div class="modal-header">
	<button 
	type="button" 
	class="close" 
	data-dismiss="modal" 
	aria-hidden="true">
	×
</button>
<h4 class="modal-title">Update Produk</h4>
</div>

<div class="modal-body">
	{!! Form::model($product, [
	'method' => 'PATCH', 
	'action' => ['ProductController@update', $product->id]
	]) !!}
	{!!	Form::label ('name', 'Nama produk baru : ', ['class' => 'control-label']) !!}
	{!!	Form::text ('name', null, ['class' => 'form-control'])	!!}

	{!!	Form::label ('price', 'Harga produk baru : ', ['class' => 'control-label']) !!}
	{!!	Form::text ('price', null, ['class' => 'form-control'])	!!}<br>
	<div class="form-footer" align="center"> 
		{{Form::button( '<i class="glyphicon glyphicon-floppy-disk"><br>Save</i>', array('type' => 'submit', 'class' => 'btn', 'style' => 'background-color:#222222; color: white; width: 100px;'))}}
	</div>
	{!! Form::close()	!!}	
</div>
