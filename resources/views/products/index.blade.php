@extends('layouts.layout')

@section('content')

<body>
    <div class="bg2">
    	<div id="category"><br>
    		<h2>&nbsp;Tambah Produk Cucian</h2>
    		{!! Form::open(['route' => ['product.store'], 'class' => 'form1'])!!}
           <div class="form-group col-md-6"> 
            {!!	Form::label ('name', 'Nama Produk : ') !!}
            {!!	Form::text ('name', null, ['class' => 'form-control','required']) !!}

            {!! Form::label ('price', 'Harga Produk : ') !!}
            {!! Form::text ('price', null, ['class' => 'form-control','required'])!!}
            
            {!! Form::label ('id_category', 'Kategori:', 
            ['class' => 'control-label'] )
            !!}
            @if (count($prod)>0)
            {!! Form::select ('id_category', $prod , null, 
            [
            'class' => 'form-control',
            'id'=> 'id_category', 
            'placeholder' => 'Pilih Kategory Produk Anda',
            'required'
            ]) 
            !!}
            @else <p>kosong</p>
            @endif
            <p> </p>
            <div align="right">
                {{Form::button( '<i class="glyphicon glyphicon-floppy-disk"><br>Save</i>', array(
                'type' => 'submit', 
                'class' => 'btn btng button1'
                ))}}
            </div>
        </div>
        {!! Form::close()!!}
    </div>
</div>

<div>
   <h2 align="center">&nbsp;List produk yang sudah terdaftar</h2>
   <table class="table" id="myTable">
    <thead>
        <tr>
            <th>Produk</th>
            <th>Kategori</th>
            <th>Harga</th>
            <th style="width: 70px">Option</th>
        </tr>
    </thead>
    <tbody>
        @foreach($datas as $data)
        <tr>
            <td>{{$data->name}}</td>
            <td>{{$data->categories->name}}</td>
            <td>{{$data->price}}</td>
            <td>
                <a 
                href="{{route('product.edit', [$data->id])}}" 
                class="btn btn-xs btng"
                data-toggle="modal" 
                data-target="#updateModal">
                <i class="glyphicon glyphicon-cog"><br>Edit</i>
            </a>
            <a 
            href="{{route('product.delete', $data->id)}}" 
            class="btn btn-xs btng" 
            data-toggle="modal"
            data-target="#deleteModal">
            <i class="glyphicon glyphicon-trash"><br>Delete</i>
        </a>
    </td>
</tr>
@endforeach
</tbody>
</table>
</div>
</body>

<script type="text/javascript">
    $(document).ready(function(){
        $('#myTable').DataTable({
            "columns": [
                null,null,null,
                { "orderable": false }
        ]});
    });
</script>
@stop