<nav class="navbar navbar-default navbar-inverse navbar-bg navbar-fixed-top">
    <div class="container">
        <div class="navbar-header ">
            <button 
            type="button" 
            class="navbar-toggle collapsed" 
            data-toggle="collapse"
            data-target="#bs-example-navbar-collapse-1"
            >
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>   
        
    </div>

    <div class="collapse navbar-collapse navbar-font" id="bs-example-navbar-collapse-1">
        
        <ul class="nav navbar-nav navbar-containercss button1 ">
            <li><a class="pull-left" href="{{ route('guest.home')}}"><img src="{{ url('/')}}/images/logokoko2.png" class="navbar-logo"></a></li>
            <li><a href="{{ route('category.index')}}">Kategori</a></li>
            <li><a href="{{ route('product.index')}}">Produk</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Order</a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ route('order.index')}}">Baru</a></li>
                    <li><a href="{{ route('order.history')}}">Riwayat</a></li>
                </ul>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            @if (Auth::guest())
            
            <li class="{{ (Request::is('/register') ? 'active' : '') }}"><a
                href="{{ route('register') }}">Register</a></li>
                @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                    aria-expanded="false"><i class="fa fa-user"></i> {{ Auth::user()->name }} <i
                    class="fa fa-caret-down"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        @if(Auth::check())
                        @if(Auth::user()->admin==1)
                        <li>
                            <a href="{{ route('admin/dashboard') }}"><i class="fa fa-tachometer"></i> Admin Dashboard</a>
                        </li>
                        @endif
                        <li role="presentation" class="divider"></li>
                        @endif
                        <li>
                            <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
    </div>
</div>
</nav>