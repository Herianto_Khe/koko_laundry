<div class="form-group"> 
    {!! Form::label ('categories', 'Kategori:', ['class' => 'control-label']) !!}
    @if (count($categories)>0)
    {!! Form::select ('categories',$categories , null, ['class' => 'form-control','id'=> 'categories', 'placeholder' => 'Pilih Kategory Produk Anda'])    !!}
    @else <p>kosong</p>
    @endif
    
    @if ($errors->has('categories'))
    <span class="help-block">{{ $errors->first('categories') }}</span>
    @endif
</div>

<div class="form-group"> 
    {!! Form::label ('products', 'Produk:', ['class' => 'control-label']) !!}
    {!! Form::text ('products', null, ['class' => 'form-control']) !!}
    @if ($errors->has('products'))
    <span class="help-block" style="color: #d20303">{{ $errors->first('products') }}</span>
    @endif
</div>

<div class="form-group"> 
	{!!	Form::label ('prices', 'Harga:', ['class' => 'control-label']) !!}
	{!!	Form::text ('prices', null, ['class' => 'form-control'])	!!}
	@if ($errors->has('prices'))
    <span class="help-block">{{ $errors->first('prices') }}</span>
    @endif
</div>

<div class="form-group"> 
	
	{{Form::button( '<i class="glyphicon glyphicon-floppy-disk"><br>Save</i>', array('type' => 'submit', 'class' => 'btn', 'style' => 'background-color:black; color: white;'))}}
	<a href="{{route('laundry.home')}}" class="btn" style="background-color:black; color: white;" >
       <i class="glyphicon glyphicon-home"><br>Return</i></a>
   </div>
