@extends('layouts.layout')
@section('content')
<style type="text/css">
	th{
		background-color: #222222;
		color: #dddddd;
		font-size: 18px;
	}
	td{
		background-color: white;
	}
</style>
<body><br>
<h2>Riwayat Pesanan</h2>
<div>
		@foreach($orders as $order)
		<table>
		<tr>
			<th style="width: 3%; border-right: 3px solid green;">{{$order->name}}</th>
			<th colspan="4">{{$order->time}}</th>
		</tr>
			@foreach($order->prints as $data)
			<tr>
				<!-- <td style="width: 30%">{{$data->id}}</td> -->
				<td style="width: 30%">{{$data->products->name}}</td>
				<td style="width: 20%">{{$data->categories->name}}</td>
				<td style="width: 20%">{{number_format($data->price)}}</td>
				<td style="width: 20%">{{number_format($data->quantity)}}</td>
				<td style="width: 20%">{{number_format($data->total)}}</td>
			</tr>
			@endforeach
			<tr hidden="true">
				<td></td>
				<td></td>
			</tr>
		</table>
		</br>	
		@endforeach
</div>
</body>
@stop