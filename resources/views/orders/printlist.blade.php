@extends('layouts.layout')
@section('content')
<style type="text/css">
	th{
		background-color: #222222;
		color: #dddddd;
	}
	td{
		background-color: white;
	}
</style>
<body>
<div><br><br>
	<table>
		<tr>
			<th>Kategori</th>
			<th>Produk</th>
			<th>Harga</th>
			<th>Jumlah</th>
			<th>Total</th>
		</tr>
		@foreach($datas as $data)
		<tr>
			<td>{{$data->categories->name}}</td>
			<td>{{$data->products->name}}</td>
			<td>{{number_format($data->price)}}</td>
			<td>{{number_format($data->quantity)}}</td>
			<td>{{number_format($data->total)}}</td>
		</tr>
		@endforeach
		<tr style="border-top: 3px solid #222222;">
			<th colspan="4" style="background-color: white; color: black;">All Total&nbsp;</th>
			<td>{{number_format($total)}}</td>
		</tr>
	</table>		

</div>
</body>
@stop