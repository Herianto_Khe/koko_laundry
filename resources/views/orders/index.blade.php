@extends('layouts.layout')
@section('content')
<body><br>
  <h2>Pesan Produk Cucian</h2>
  <div class="order-bg2">
    <button class="btn buttonc" onclick="AddOrder();">Add New Order</button><p></p>
    {!! Form::open(['route' => 'order.print'])!!}
      {!! Form::label (
        'orderid', 
        'Order Number : ', 
        ['class' => 'control-label']) 
      !!}
      {!! Form::text (
        'orderid', 
        null, 
        ['class' => 'Order-box','required'])  
    !!}
    <table>
      <tbody id="tbody">
        <tr>
          <th style="width: 215px">Kategori</th>
          <th style="width: 200px">Produk</th>
          <th style="width: 240px">Harga</th>
          <th style="width: 50px">Jumlah</th>
          <th style="width: 250px">Total</th>
          <th> </th>
        </tr>
      </tbody>
    </table>
    <div align="right">
      <p></p>
      {{Form::button( 
      '<i class="glyphicon glyphicon-print"><br>Print</i>', array(
      'type'  => 'submit', 
      'class' => 'btn buttonc'
      ))}}
      {!! Form::close()   !!}
    </div>
  </div>
</body>


  <script type="text/javascript">        
    function AddOrder(){
      //-------tambah row---------    
      var row = 
      
      '<tr>'+
      '<td>'+
      '<select class="category form-control" name="category[]" required>'+
      '<option value="0" selected="true" disabled="true">Pilih Kategory Produk</option>'+   
      '@foreach($datac as  $key => $d1)'+
      '<option value="{!!$d1->id!!}">{!!$d1->name!!}</option>'+
      '@endforeach'+    
      '</select>'+
      '</td>'+
      '<td>'+
      '<select class="product form-control" name="product[]" required>'+
      '<option  value="0" selected="true" disabled="true">Pilih Nama Produk</option>'+    
      '</select>'+
      '</td>'+
      '<td>'+
      '<input type="text" readonly="true" class="price form-control "  name="price[]" />'+
      '</td>'+
      '<td>'+
      '<input type="text" class="jumlah form-control" name="quantity[]"/>'+
      '</td>'+
      '<td>'+
      '<input type="text" readonly="true" class="count-total form-control" name="total[]"/>'+
      '</td>'+
      '<td>'+
      '<a href="javascript:void(0)" onclick="javascript:deleteRow(this)" class="btn"><i style="color:#ffffff;" class="glyphicon glyphicon-remove"></i></a>'+
      '</td>'+
      '</tr>';
      
      $("#tbody").append(row);    
    };

    function deleteRow(btn) {
      var row = btn.parentNode.parentNode;
      row.parentNode.removeChild(row);
    };

    //ini cari product
    $('#tbody').delegate(".category","change",function(){
     
      var row = $(this).parent().parent();
      var id = row.find('.category').val();
      var pro = row.find('.product');
      var dataId = {'id':id};
      $.ajax({    
        type : 'GET',
        url : '{!!URL::route("findProduct")!!}',
        dataType : 'json',
        data : dataId,
        success : function(data)
        {
          pro.empty();
          pro.append("<option value='0' selected='true' disabled='true'>Pilih Nama Produk</option>");  
          $(data).each(function(index){
            var opt = "<option value="+data[index].id+">"+data[index].name+"</option>";
            pro.append(opt);
          });
        }
      });      
    });
    //----------------------------

     //ini cari product
     $('#tbody').delegate(".product","change",function(){
       
      var row = $(this).parent().parent();
      var id = row.find('.product').val();
      var pri = row.find('.price');
      var dataId = {'id':id};
      $.ajax({    
        type : 'GET',
        url : '{!!URL::route("findPrice")!!}',
        dataType : 'json',
        data : dataId,
        success : function(data)
        {
          pri.val(data.price);
        }
      });      
    });
    //----------------------------

    $('tbody').on('keyup','.jumlah',function(){
      var tr = $(this).parent().parent();
      var price = tr.find('.price').val();
      var jumlah = tr.find('.jumlah').val();
      tr.find('.count-total').val(price * jumlah);
    });

    $('tbody').on('change','.category',function(){
     var tr = $(this).parent().parent();
     tr.find('.count-total').val(0);
     tr.find('.price').val(0);
     tr.find('.jumlah').val(0);
   });
 </script>

 @stop