@extends('layouts.layout')
@include('popup')
@section('content')
<style type="text/css">
    body {
        background: #222222;            
    }
    .buttonbg{
        background:#1c4a47;
        width: auto;
        color: white;
        margin: auto;
        font-size: 22px;
        filter: drop-shadow(5px 5px 7px black);
    }
    .image{
        display: block; 
        width: 50%;
        margin: auto;
        margin-top: 10%;
        opacity: 0.7;
        filter: drop-shadow(1px 1px 2px white);
    }      
</style>

<div>

    <img class="image" src="{{ url('/')}}/images/logokoko2.png">
    <div align="center">
        @if (Auth::guest())
        <a class="buttonbg btn"  href="{{ url('/public/login') }}" data-toggle="modal" data-target="#loginModal">Log In
        </a> 
        @else
        <a type="hidden"></a>
        @endif  
    </div>   
</div>

@endsection
