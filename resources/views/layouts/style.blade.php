<style type="text/css">
    body{
      background-color:#bbbbbb;
      color:#333333;
      margin-top: 80px;
    }
    table{
      width: 100%;
    }
    td,th{
      border-top: 0px solid #ddd;
      padding: 15px;
      text-align: left;
    }
    tr:nth-child(even) {background-color: #444444}
    thead{
      background-color: #222222;
      color: #dddddd;
    }
    .image {
      display:block;
      width:50%;
      margin:auto;
      margin-top:10%;
      opacity:0.7;
      filter:drop-shadow(1px 1px 2px white);
    }
    /* this is style for categories index */ 
    .bg {
      background-color:#222222;
      color:white;
      height:190px;
    }
    .btng {
      background-color:#1c4a47;
      color:white;
    }
    /*Style for Categories Page*/
    
    .bg2 {
      background-color:#222222;
      color:white;
      height: 330px;
    }
    .image{
      display: block;     
      width: 50%;
      margin: auto;
      margin-top: 10%;
      opacity: 0.7;
      filter: drop-shadow(1px 1px 2px white);
    } 
    .buttonc{
      background-color: #1c4a47; 
      color:white;
    }

    /*Style for Order Page*/
    .order-bg2{
      background-color: #222222;
      color: white;
      padding-left: 2%;
      padding-right: 2%;
      padding-top: 2%;
      padding-bottom: 3%;
    }
    .Order-box{
      width: 50px;
      color: black;
    }

    /*Style for Navbar*/
    .navbar-bg {
        background:
        -webkit-linear-gradient(45deg, hsla(340, 0%, 13%, 1) 0%, hsla(340, 0%, 13%, 0) 70%),
        -webkit-linear-gradient(315deg, hsla(225, 0%, 32%, 1) 10%, hsla(225, 0%, 32%, 0) 80%),
        -webkit-linear-gradient(225deg, hsla(140, 0%, 11%, 1) 10%, hsla(140, 0%, 11%, 0) 80%),
        -webkit-linear-gradient(135deg, hsla(35, 0%, 29%, 1) 100%, hsla(35, 0%, 29%, 0) 70%);
        background:
        linear-gradient(45deg, hsla(340, 0%, 13%, 1) 0%, hsla(340, 0%, 13%, 0) 70%),
        linear-gradient(135deg, hsla(225, 0%, 32%, 1) 10%, hsla(225, 0%, 32%, 0) 80%),
        linear-gradient(225deg, hsla(140, 0%, 11%, 1) 10%, hsla(140, 0%, 11%, 0) 80%),
        linear-gradient(315deg, hsla(35, 0%, 29%, 1) 100%, hsla(35, 0%, 29%, 0) 70%);
    }
    .navbar-logo{
        height: 90px;
        filter: drop-shadow(0.5px 0.5px 1px white);
    }
    .navbar-font{
        font-size: 25px;
        margin-top:0.5%;
        filter: drop-shadow(0.5px 0.5px 0.7px white);
    }
    .navbar-containercss{
        height: 60px;
    }

  </style>