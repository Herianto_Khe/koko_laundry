<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Koko Laundry</title>
  <!-- Styles -->
  <!-- <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet"> -->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
  <script src="{{ asset('js/jquery.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
  @stack('script') 
  @yield('style') 
  @include('nav')
  @include('layouts.style') 
  @include('categories/popup') 
  @include('products/popup') 
  <!-- Scripts -->
  
</head>
<body>
  <div id="app" class="content container">
   @yield('content') <br></div>
   <script type="text/javascript">
    $('.category').on('change', function() {
      alert( 'asdf' );
    })
    $('.button1').click(function(){
      $('.form1').submit(function(){
        $('.button1').prop('disabled',true);
      });
    });
  </script>
</body>
</html>