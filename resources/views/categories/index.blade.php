@extends('layouts.layout')

@section('content')
<body>
    <div class="bg">
        <div id="category"><br>
        	<h2>&nbsp;Tambah Kategori Cucian</h2>
            {!! Form::open(['route' => ['category.store'], 'class' => 'form1'])!!}
            <div class="form-group col-md-6 " > 
             {!!	Form::label (
             'name', 
             'Kategori Baru : ', 
             ['class' => 'control-label']) 
             !!}
             {!!	Form::text (
             'name', 
             null, 
             ['class' => 'form-control',
             'required'])	
             !!}
         </div><br>
         <div class="form-group"> 
             {{Form::button( '
             <i class="glyphicon glyphicon-floppy-disk"><br>Save</i>', array(
             'type' => 'submit', 
             'class' => 'btn btng button1'
             ))}}
         </div>
         {!! Form::close() !!}
     </div>
 </div>

 <div>
   <h2 align="center">&nbsp;List kategori yang sudah terdaftar</h2>
   <table 
   class="table" 
   id="categories-table" 
   >
   <thead>
    <tr>
        <th>Kategori</th>
        <th style="width: 120px">Option</th>
    </tr>
</thead>
<tbody>
    @foreach($datas as $data)
    <tr>
        <td>{{$data->name}}</td>
        <td >
            <a 
            href="{{route('category.edit', [$data->id])}}" 
            class="btn btn-sm btng"
            data-toggle="modal" 
            data-target="#updateModal">
            <i class="glyphicon glyphicon-cog"><br>Edit</i>
        </a>
        <a 
        href="{{route('category.delete',[$data->id])}}"
        class="btn btn-sm btng"
        data-toggle="modal"
        data-target="#deleteModal">
        <i class="glyphicon glyphicon-trash"><br>Delete</i>
    </a>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</body>

<script type="text/javascript">
    $(document).ready(function(){
        $('#categories-table').dataTable({
            "columns": [
                null,
                { "orderable": false }
        ]});
    });
</script>

@stop

