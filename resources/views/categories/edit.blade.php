<div class="modal-header">
	<button 
	type="button" 
	class="close" 
	data-dismiss="modal" 
	aria-hidden="true">
	×
</button>
<h4 class="modal-title">Update Kategori</h4>
</div>

<div class="modal-body">
	<p>Silahkan memasukan nama kategori baru :</p>
	{!! Form::model($category, [
	'method' => 'PATCH', 
	'action' => ['CategoryController@update', $category->id]]) 
	!!}
	{!!	Form::text ('name', null, ['class' => 'form-control', 'required' ])	!!}<br>
	<div class="modal-footer" style="height: 40px;">
		{{Form::button(
		'<i class="glyphicon glyphicon-floppy-disk"> Save</i>', 
		array('type' => 'submit', 'class' => 'btn btn-md btng')
		)}}
	</div>
	{!! Form::close()!!}
</div>

